<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Ciclista;
use app\models\Lleva;
use app\models\Puerto;
use yii\data\ActiveDataProvider;
use yii\data\SqlDataProvider;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }
    
    // Consulta 1
    // Consulta 1 Active Record
    public function actionConsulta1ar(){
        
        $dataProvider = new ActiveDataProvider([
            'query' => Ciclista::find()->select("edad")->distinct(),
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['edad'],
            "titulo"=>"Consulta 1 con Active Record",
            "enunciado"=>"Listar las edades de los ciclistas sin repetidos",
            "sql"=>"SELECT DISTINCT edad FROM ciclista",
        ]);
    }
    
    // Consulta 1 DAO
    public function actionConsulta1dao(){
        
        $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT DISTINCT edad FROM ciclista'
       ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['edad'],
            "titulo"=>"Consulta 1 con DAO",
            "enunciado"=>"Listar las edades de los ciclistas sin repetidos",
            "sql"=>"SELECT DISTINCT edad FROM ciclista",
        ]);
    }
    
    // Consulta 2
    // Consulta 2 Active Record
    public function actionConsulta2ar(){
        
        $dataProvider = new ActiveDataProvider([
            'query' => Ciclista::find()->select("edad")->distinct()->where("nomequipo = 'Artiach'"),
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['edad'],
            "titulo"=>"Consulta 2 con Active Record",
            "enunciado"=>"Listar las edades de los ciclistas de Artiach o de Amore Vita",
            "sql"=>"SELECT DISTINCT edad FROM ciclista WHERE nomequipo = 'artiach'",
        ]);
    }
    
    // Consulta 2 DAO
    public function actionConsulta2dao(){
        
        $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT DISTINCT edad FROM ciclista WHERE nomequipo = "artiach"',
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['edad'],
            "titulo"=>"Consulta 2 con DAO",
            "enunciado"=>"Listar las edades de los ciclistas de Artiach o de Amore Vita",
            "sql"=>"SELECT DISTINCT edad FROM ciclista WHERE nomequipo = 'artiach'",
        ]);
    }

    // Consulta 3
    // Consulta 3 Active Record
    public function actionConsulta3ar(){
        
        $dataProvider = new ActiveDataProvider([
            'query' => Ciclista::find()->select("edad")->distinct()->where("nomequipo = 'artiach' OR nomequipo = 'amore vita'"),
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['edad'],
            "titulo"=>"Consulta 3 con Active Record",
            "enunciado"=>"Listar las edades de los ciclistas de Artiach o de Amore Vita",
            "sql"=>"SELECT DISTINCT edad FROM ciclista WHERE nomequipo = 'artiach' OR nomequipo = 'amore vita';",
        ]);
    }
    
    // Consulta 3 DAO
    public function actionConsulta3dao(){
        
        $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT DISTINCT edad FROM ciclista WHERE nomequipo = "artiach" OR nomequipo = "amore vita"',
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['edad'],
            "titulo"=>"Consulta 3 con DAO",
            "enunciado"=>"Listar las edades de los ciclistas de Artiach o de Amore Vita",
            "sql"=>"SELECT DISTINCT edad FROM ciclista WHERE nomequipo = 'artiach' OR nomequipo = 'amore vita';",
        ]);
    }
    
    // Consulta 4
    // Consulta 4 Active Record
    public function actionConsulta4ar(){
        
        $dataProvider = new ActiveDataProvider([
            'query' => Ciclista::find()->select("dorsal")->where("edad < 25 OR edad > 30"),
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=>"Consulta 4 con Active Record",
            "enunciado"=>"Listar los dorsales de los ciclistas cuya edad sea menor que 25 o mayor que 30",
            "sql"=>"SELECT dorsal FROM ciclista WHERE edad < 25 OR edad > 30;",
        ]);
    }
    
    // Consulta 4 DAO
    public function actionConsulta4dao(){
        
        $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT dorsal FROM ciclista WHERE edad < 25 OR edad > 30',
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=>"Consulta 4 con DAO",
            "enunciado"=>"Listar los dorsales de los ciclistas cuya edad sea menor que 25 o mayor que 30",
            "sql"=>"SELECT dorsal FROM ciclista WHERE edad < 25 OR edad > 30;",
        ]);
    }
    
    // Consulta 5
    // Consulta 5 Active Record
    public function actionConsulta5ar(){
        
        $dataProvider = new ActiveDataProvider([
            'query' => Ciclista::find()->select("dorsal")->where("(edad BETWEEN 28 AND 32) AND nomequipo = 'banesto'"),
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=>"Consulta 5 con Active Record",
            "enunciado"=>"Listar los dorsales de los ciclistas cuya edad esté entre 28 y 32 y además que sólo sean de Banesto",
            "sql"=>"SELECT dorsal FROM ciclista WHERE (edad BETWEEN 28 AND 32) AND nomequipo = 'banesto';",
        ]);
    }
    
    // Consulta 5 DAO
    public function actionConsulta5dao(){
        
        $dataProvider = new SqlDataProvider([
            'sql'=>"SELECT dorsal FROM ciclista WHERE (edad BETWEEN 28 AND 32) AND nomequipo = 'banesto'",
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=>"Consulta 5 con DAO",
            "enunciado"=>"Listar los dorsales de los ciclistas cuya edad esté entre 28 y 32 y además que sólo sean de Banesto",
            "sql"=>"SELECT dorsal FROM ciclista WHERE (edad BETWEEN 28 AND 32) AND nomequipo = 'banesto';",
        ]);
    }
    
    // Consulta 6
    // Consulta 6 Active Record
    public function actionConsulta6ar(){
        
        $dataProvider = new ActiveDataProvider([
            'query' => Ciclista::find()->select("nombre")->distinct()->where("CHAR_LENGTH(nombre) > 8"),
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['nombre'],
            "titulo"=>"Consulta 6 con Active Record",
            "enunciado"=>"Indícame el nombre de los ciclistas que el número de caracteres del nombre sea mayor que 8",
            "sql"=>"SELECT DISTINCT nombre FROM ciclista WHERE CHAR_LENGTH(nombre) > 8;",
        ]);
    }
    
    // Consulta 6 DAO
    public function actionConsulta6dao(){
        
        $dataProvider = new SqlDataProvider([
            'sql'=>"SELECT DISTINCT nombre FROM ciclista WHERE CHAR_LENGTH(nombre) > 8",
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['nombre'],
            "titulo"=>"Consulta 6 con DAO",
            "enunciado"=>"Indícame el nombre de los ciclistas que el número de caracteres del nombre sea mayor que 8",
            "sql"=>"SELECT DISTINCT nombre FROM ciclista WHERE CHAR_LENGTH(nombre) > 8;",
        ]);
    }
    
    // Consulta 7
    // Consulta 7 Active Record
    public function actionConsulta7ar(){
        
        $dataProvider = new ActiveDataProvider([
            'query' => Ciclista::find()->select("dorsal, nombre, UPPER(nomequipo) AS nombre_mayusculas"),
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal','nombre','nombre_mayusculas'],
            "titulo"=>"Consulta 7 con Active Record",
            "enunciado"=>"Lístame el nombre y el dorsal de todos los ciclistas mostrando un campo nuevo denominado nombre mayúsculas que debe mostrar el nombre en mayúsculas",
            "sql"=>"SELECT DISTINCT nombre, dorsal,UPPER(nombre) AS 'nombre mayusculas' FROM ciclista;",
        ]);
    }
    
    // Consulta 7 DAO
    public function actionConsulta7dao(){
        
        $dataProvider = new SqlDataProvider([
            'sql'=>"SELECT DISTINCT nombre, dorsal,UPPER(nombre) AS 'nombre_mayusculas' FROM ciclista",
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal','nombre','nombre_mayusculas'],
            "titulo"=>"Consulta 7 con DAO",
            "enunciado"=>"Lístame el nombre y el dorsal de todos los ciclistas mostrando un campo nuevo denominado nombre mayúsculas que debe mostrar el nombre en mayúsculas",
            "sql"=>"SELECT DISTINCT nombre, dorsal,UPPER(nombre) AS 'nombre mayusculas' FROM ciclista;",
        ]);
    } 
    
    // Consulta 8
    // Consulta 8 Active Record
    public function actionConsulta8ar(){
        
        $dataProvider = new ActiveDataProvider([
            'query' => Lleva::find()->select("dorsal")->distinct()->where("código = 'mge'"),
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=>"Consulta 8 con Active Record",
            "enunciado"=>"Listar todos los ciclistas que han ganado el maillot MGE (amarillo) en alguna etapa",
            "sql"=>"SELECT DISTINCT dorsal FROM lleva WHERE código = 'mge';",
        ]);
    }
    
    // Consulta 8 DAO
    public function actionConsulta8dao(){
        
        $dataProvider = new SqlDataProvider([
            'sql'=>"SELECT DISTINCT dorsal FROM lleva WHERE código = 'mge'",
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=>"Consulta 8 con DAO",
            "enunciado"=>"Listar todos los ciclistas que han ganado el maillot MGE (amarillo) en alguna etapa",
            "sql"=>"SELECT DISTINCT dorsal FROM lleva WHERE código = 'mge';",
        ]);
    }
       
    // Consulta 9
    // Consulta 9 Active Record
    public function actionConsulta9ar(){
        
        $dataProvider = new ActiveDataProvider([
            'query' => Puerto::find()->select("nompuerto")->where("altura > 1500"),
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['nompuerto'],
            "titulo"=>"Consulta 9 con Active Record",
            "enunciado"=>"Listar el nombre de los puertos cuya altura sea mayor de 1500 ",
            "sql"=>"SELECT nompuerto FROM puerto WHERE altura > 1500;",
        ]);
    }
    
    // Consulta 9 dao
    public function actionConsulta9dao(){
        
        $dataProvider = new SqlDataProvider([
            'sql'=>"SELECT nompuerto FROM puerto WHERE altura > 1500",
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['nompuerto'],
            "titulo"=>"Consulta 9 con DAO",
            "enunciado"=>"Listar el nombre de los puertos cuya altura sea mayor de 1500 ",
            "sql"=>"SELECT nompuerto FROM puerto WHERE altura > 1500;",
        ]);
    }
    
    // Consulta 10
    // Consulta 10 Active Record
    public function actionConsulta10ar(){
        
        $dataProvider = new ActiveDataProvider([
            'query' => Puerto::find()->select("dorsal")->distinct()->where("pendiente > 8 OR (altura BETWEEN 1800 AND 3000)"),
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=>"Consulta 10 con Active Record",
            "enunciado"=>"Listar el dorsal de los ciclistas que hayan ganado algun puerto cuya pendiente sea mayor que 8 o cuya altura esté entre 1800 y 3000",
            "sql"=>"SELECT DISTINCT dorsal FROM puerto WHERE pendiente > 8 OR (altura BETWEEN 1800 AND 3000);",
        ]);
    }
    
    // Consulta 10 DAO
    public function actionConsulta10dao(){
        
        $dataProvider = new SqlDataProvider([
            'sql'=>"SELECT DISTINCT dorsal FROM puerto WHERE pendiente > 8 OR (altura BETWEEN 1800 AND 3000)",
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=>"Consulta 10 con DAO",
            "enunciado"=>"Listar el dorsal de los ciclistas que hayan ganado algun puerto cuya pendiente sea mayor que 8 o cuya altura esté entre 1800 y 3000",
            "sql"=>"SELECT DISTINCT dorsal FROM puerto WHERE pendiente > 8 OR (altura BETWEEN 1800 AND 3000);",
        ]);
    }
    
    // Consulta 11
    // Consulta 11 Active Record
    public function actionConsulta11ar(){ 
        
        $dataProvider = new ActiveDataProvider([
            'query' => Puerto::find()->select("dorsal")->distinct()->where("pendiente > 8 AND (altura BETWEEN 1800 AND 3000)"),
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=>"Consulta 11 con Active Record",
            "enunciado"=>"Listar el dorsal de los ciclistas que hayan ganado algún puerto cuya pendiente sea mayor que 8 y cuya altura esté entre 1800 y 3000",
            "sql"=>"SELECT DISTINCT dorsal FROM puerto WHERE pendiente > 8 AND (altura BETWEEN 1800 AND 3000);",
        ]);
    }
    
    // Consulta 11 DAO
    public function actionConsulta11dao(){ 
        
        $dataProvider = new SqlDataProvider([
            'sql'=>"SELECT DISTINCT dorsal FROM puerto WHERE pendiente > 8 AND (altura BETWEEN 1800 AND 3000)",
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=>"Consulta 11 con DAO",
            "enunciado"=>"Listar el dorsal de los ciclistas que hayan ganado algún puerto cuya pendiente sea mayor que 8 y cuya altura esté entre 1800 y 3000",
            "sql"=>"SELECT DISTINCT dorsal FROM puerto WHERE pendiente > 8 AND (altura BETWEEN 1800 AND 3000);",
        ]);
    }
    
    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
}
